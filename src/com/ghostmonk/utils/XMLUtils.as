package com.ghostmonk.utils
{
	import mx.messaging.management.Attribute;

	public class XMLUtils
	{
		public static function XMLListToArray( list:XMLList ) : Array
		{
			var output:Array = [];
			for each( var node:XML in list )
			{
				output.push( node.toString() );
			}
			return output;
		}
		
		public static function SimpleXmlToObject( xml:XML ) : Object
		{
			var output:Object = {};
			for each( var node:XML in xml.children() ) 
			{
				output[ node.localName() ] = ConvertValue( node.toString() );
			}
			
			for each( var attribute:XML in xml.attributes() ) 
			{
				output[ attribute.localName() ] = ConvertValue( attribute.toString() );
			}
			return output;
		}
		
		private static function ConvertValue( value:String ) : *
		{
			if( value == "true" || value == "false" )
				return value == "true" ? true : false;
			
			var num:Number = Number( value );
			if( !isNaN( num ) ) 
				return num;
			
			return value;
		}
	}
}